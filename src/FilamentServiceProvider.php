<?php

namespace KDA\Filament\DynamicNavigation;

use Filament\Facades\Filament;
use Filament\Navigation\NavigationBuilder;
use Filament\Navigation\NavigationGroup;
use Filament\Navigation\NavigationItem;
use Filament\PluginServiceProvider;
use KDA\Filament\DynamicNavigation\Models\NavigationGroup as ModelsNavigationGroup;
use KDA\Filament\DynamicNavigation\Models\NavigationItem as ModelsNavigationItem;
use KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
        //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
        //    CustomWidget::class,
    ];

    protected array $pages = [
        //    CustomPage::class,
    ];

    protected array $resources = [
        //     CustomResource::class,
        //  \KDA\Filament\DynamicNavigation\Resources\NavigationGroupResource::class,
        //  \KDA\Filament\DynamicNavigation\Resources\NavigationItemResource::class
        DynamicNavigationGroupResource::class
    ];

    public function configurePackage(Package $package): void
    {
        $package->name('filament-dynamic-navigation');
    }

    public function packageBooted(): void
    {
        parent::packageBooted();



        Filament::navigation(function (NavigationBuilder $builder): NavigationBuilder {
            $groups =  ModelsNavigationGroup::with('items')->orderBy('sort')->get()->reduce(function ($carry, $group) {
                
                    $items = $group->items->where('use_navigation_items', 1)->flatMap(function ($item) {
                        $class = $item->resource ?? $item->page;
                        if ($class && class_exists($class,false)) {
                            return $class::getNavigationItems();
                        }
                    })->toArray();

                    $itemsurl = $group->items->whereNotNull('url' )->map(function ($item) {
                        $nav =  NavigationItem::make($item->label)->url($item->url)->openUrlInNewTab($item->open_in_new_tab)->icon($item->icon);
                        return $nav;
                     })->toArray();
                     $elements = array_merge($items,$itemsurl);

                    if (count($elements) > 0) {
                        $carry->push(
                            NavigationGroup::make(__($group->name))->items($elements)
                        );
                    }
                    
                return $carry;
            }, collect([]))->toArray();
            if(count($groups)==0){
                $groups[]=  NavigationGroup::make('Setup')->items([...DynamicNavigationGroupResource::getNavigationItems()]);
            }
            return $builder->items([
             /*   NavigationItem::make('Dashboard')
                    ->icon('heroicon-o-home')
                    ->isActiveWhen(fn (): bool => request()->routeIs('filament.pages.dashboard'))
                    ->url(route('filament.pages.dashboard')),*/

            ])->groups($groups/*[
                NavigationGroup::make(__('filament.navigation.groups.content'))->items([
                    ...PostResource::getNavigationItems(),
                    ...PageResource::getNavigationItems(),
                    ...TagResource::getNavigationItems(),
                    ...SluggableResource::getNavigationItems(),
                    ...ManageImportantInfo::getNavigationItems(),
                    ...ManageContact::getNavigationItems()
                ]),

                NavigationGroup::make(__('filament.navigation.groups.navigation'))->items([
                    ...ManageNavigation::getNavigationItems(),
                    ...NavigationResource::getNavigationItems(),
                ]),
                NavigationGroup::make(__('filament.navigation.groups.media'))->items([
                    ...MedialibraryResource::getNavigationItems(),
                ]),
                NavigationGroup::make(__('filament.navigation.groups.seo'))->items([
                    ...ManageSeo::getNavigationItems(),
                    ...SeoResource::getNavigationItems(),
                ]),
                NavigationGroup::make(__('filament.navigation.groups.access'))->items([
                    ...UserResource::getNavigationItems(),
                ]),
            ]*/);
        });
    }
}
