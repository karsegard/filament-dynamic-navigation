<?php
namespace KDA\Filament\DynamicNavigation;

use Filament\Facades\Filament;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasDumps;
use \KDA\Filament\DynamicNavigation\Models\NavigationGroup;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasMigration;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasProviders;
    use \KDA\Laravel\Traits\HasTranslations;
    use HasDumps;

    protected $packageName='filament-dynamic-navigation';

    protected $dumps=[
        'filament_navigation_groups',
        'filament_navigation_items'
    ];
    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
  
    protected $configDir='config';
    protected $configs = [
         'kda/filament-dynamic-navigation.php'  => 'kda.filament-dynamic-navigation'
    ];
    protected $additionnalProviders=[
        \KDA\Filament\DynamicNavigation\FilamentServiceProvider::class
    ];
    public function postRegister(){
    }
    protected function bootSelf(){
        Filament::serving(function(){
            $groups = NavigationGroup::orderBy('sort','asc')->get()->pluck('name')->toArray();
            Filament::registerNavigationGroups($groups);
            
        });
    }
}
