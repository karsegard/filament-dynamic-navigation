<?php

namespace KDA\Filament\DynamicNavigation\Resources\NavigationItemResource\Pages;

use Filament\Facades\Filament;
use KDA\Filament\DynamicNavigation\Resources\NavigationItemResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageNavigationItems extends ManageRecords
{
    protected static string $resource = NavigationItemResource::class;


    protected function getTableReorderColumn(): ?string
    {
        return 'sort';
    }
    protected function getDefaultTableSortColumn(): ?string
    {
        return 'sort';
    }
     
    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
           
        ];
    }
}
