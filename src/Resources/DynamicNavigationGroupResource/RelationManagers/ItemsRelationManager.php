<?php

namespace KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource\RelationManagers;

use Closure;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Facades\Filament;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Tables\Actions\Action;
use Filament\Tables\Actions\BulkAction;
use Illuminate\Support\HtmlString;
use KDA\Filament\DynamicNavigation\Models\NavigationGroup;
use ReflectionClass;

class ItemsRelationManager extends RelationManager
{
    protected static string $relationship = 'items';

    protected static ?string $recordTitleAttribute = 'name';

    public static function callbackForSelectToggle($only,$fields):Closure
    {
        return function ($state,$set) use ($fields,$only){
            if($state){
                collect($fields)->filter(fn($item)=>$item != $only)->map(fn($item)=>$set($item,false));
            }
        };
    }

    public static function form(Form $form): Form
    {
        $resources = collect(Filament::getResources())->mapWithKeys(function ($item) {
            $class = new ReflectionClass($item);
            return [$class->getName() => $class->getShortName()." (<span class=\"text-sm\">".$class->getName()."</span>)" ];
        });
        $pages = collect(Filament::getPages())->mapWithKeys(function ($item) {
            $class = new ReflectionClass($item);
            return [$class->getName() => $class->getShortName()];
        });
        $toggles = ['use_url','registered_page','registered_resource'];
        return $form
            ->schema([
                TextInput::make('label'),
                Grid::make(2)->schema([
                    Toggle::make('registered_resource')->reactive()
                    ->afterStateUpdated(static::callbackForSelectToggle('registered_resource',$toggles))
                        ->afterStateHydrated(fn ($get, $set) => $get('resource') ? $set('registered_resource', true) : null),
                    Select::make('resource')->allowHtml()->options($resources)->visible(fn ($get) => $get('registered_resource'))->searchable(),
                ])->columnSpanFull(),
                Grid::make(2)->schema([
                    Toggle::make('registered_page')->reactive()
                        ->afterStateUpdated(static::callbackForSelectToggle('registered_page',$toggles))
                        ->afterStateHydrated(fn ($get, $set) => $get('page') ? $set('registered_page', true) : null),
                    Select::make('page')->options($pages)->visible(fn ($get) => $get('registered_page')),
                ])->columnSpanFull(),
                Grid::make(3)->schema([
                    Toggle::make('use_url')->reactive()
                        ->afterStateUpdated(static::callbackForSelectToggle('use_url',array_merge($toggles,['use_navigation_items'])))
                        ->afterStateHydrated(fn ($get, $set) => $get('url') ? $set('use_url', true) : null),
                    TextInput::make('url')->visible(fn ($get) => $get('use_url')),
                    Toggle::make('open_in_new_tab')->visible(fn ($get) => $get('use_url')),
                ])->columnSpanFull(),
                TextInput::make('icon')->required(fn($get)=> $get('use_url')),
                Toggle::make('use_navigation_items')->default(true),

            ]);
    }

    protected function getTableReorderColumn(): ?string
    {
        return 'sort';
    }
    protected function getDefaultTableSortColumn(): ?string
    {
        return 'sort';
    }
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('label'),
                Tables\Columns\TextColumn::make('name')->label('Resource or page'),
                Tables\Columns\TextColumn::make('class_exists'),
                
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
                BulkAction::make('move')->action(function($records,$data){
                    $records->each(function($record) use ($data){
                        $record->group_id=$data['group_id'];
                        $record->save();
                    });
                })
                ->form([
                    Select::make('group_id')->options(NavigationGroup::all()->pluck('name','id'))
                ])
            ]);
    }
}
