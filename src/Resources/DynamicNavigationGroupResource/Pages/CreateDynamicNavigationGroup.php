<?php

namespace KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource\Pages;

use KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateDynamicNavigationGroup extends CreateRecord
{
    protected static string $resource = DynamicNavigationGroupResource::class;
}
