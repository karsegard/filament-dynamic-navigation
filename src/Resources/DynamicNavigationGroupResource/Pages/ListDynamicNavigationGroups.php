<?php

namespace KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource\Pages;

use KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource;
use Filament\Pages\Actions;
use Filament\Pages\Actions\Action;
use Filament\Resources\Pages\ListRecords;
use Filament\Facades\Filament;
use ReflectionFunction;

class ListDynamicNavigationGroups extends ListRecords
{
    protected static string $resource = DynamicNavigationGroupResource::class;

    protected function getTableReorderColumn(): ?string
    {
        return 'sort';
    }
    protected function getDefaultTableSortColumn(): ?string
    {
        return 'sort';
    }
     
    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Action::make('Collect existing groups')->color('danger')->requiresConfirmation()->action(function () { 
                $model = $this->getModel();
                collect(Filament::getNavigation())->map(function($group){
                    return invade($group)->items->map(function($item){
                        dump($item);
                        $fn = new ReflectionFunction(invade($item)->isActiveWhen);
                        //dump($fn->getClosureUsedVariables(),$fn->getClosureScopeClass(),$fn->get);
                    });
                });
                $groups = collect(Filament::getNavigation())->keys()->toArray();
               // $model::where('collected',true)->delete();
                foreach($groups as $group){
                    if(!empty($group)){
                        $model::firstOrCreate(['name'=>$group,'collected'=>true]);
                    }
                }
            })
        ];
    }
}
