<?php

namespace KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource\Pages;

use KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditDynamicNavigationGroup extends EditRecord
{
    protected static string $resource = DynamicNavigationGroupResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
