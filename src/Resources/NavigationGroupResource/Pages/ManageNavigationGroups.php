<?php

namespace KDA\Filament\DynamicNavigation\Resources\NavigationGroupResource\Pages;

use Filament\Facades\Filament;
use KDA\Filament\DynamicNavigation\Resources\NavigationGroupResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageNavigationGroups extends ManageRecords
{
    protected static string $resource = NavigationGroupResource::class;


    protected function getTableReorderColumn(): ?string
    {
        return 'sort';
    }
    protected function getDefaultTableSortColumn(): ?string
    {
        return 'sort';
    }
     
    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\Action::make('Collect existing groups')->color('danger')->requiresConfirmation()->action(function () { 
                $model = $this->getModel();
                $groups = collect(Filament::getNavigation())->keys()->toArray();
               // $model::where('collected',true)->delete();
                foreach($groups as $group){
                    if(!empty($group)){
                        $model::firstOrCreate(['name'=>$group,'collected'=>true]);
                    }
                }
            })
        ];
    }
}
