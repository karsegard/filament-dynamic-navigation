<?php

namespace KDA\Filament\DynamicNavigation\Resources;

use KDA\Filament\DynamicNavigation\Resources\NavigationItemResource\Pages;
use KDA\Filament\DynamicNavigation\Models\NavigationItem as Model;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Select;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Facades\Filament;
use ReflectionClass;

class NavigationItemResource extends Resource
{
    protected static ?string $model = Model::class;

    protected static ?string $navigationIcon = 'heroicon-o-view-list';

    public static function form(Form $form): Form
    { 
        $resources= collect(Filament::getResources())->mapWithKeys(function($item){
            $class = new ReflectionClass($item);
            return [$class->getName()=>$class->getShortName()];
        });
        return $form
            ->schema([
                //
                Select::make('resource')->options($resources),
            ]);
    }

    public static function table(Table $table): Table
    {
       
        return $table
            ->columns([
                //
                TextColumn::make('name')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getPages(): array
    {
        
        return [
            'index' => Pages\ManageNavigationItems::route('/'),
        ];
    }    

    protected static function getNavigationGroup(): ?string
    {
        return config('kda.filament-dynamic-navigation.nav.items.group', true)
            ? __(config('kda.filament-dynamic-navigation.nav.items.group_lang_key'))
            : '';
    }
}
