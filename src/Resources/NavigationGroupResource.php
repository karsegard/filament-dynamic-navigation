<?php

namespace KDA\Filament\DynamicNavigation\Resources;

use KDA\Filament\DynamicNavigation\Resources\NavigationGroupResource\Pages;
use KDA\Filament\DynamicNavigation\Models\NavigationGroup;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class NavigationGroupResource extends Resource
{
    protected static ?string $model = NavigationGroup::class;

    protected static ?string $navigationIcon = 'heroicon-o-view-list';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
                TextInput::make('name')
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                TextColumn::make('name')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageNavigationGroups::route('/'),
        ];
    }    

    protected static function getNavigationGroup(): ?string
    {
        return config('kda.filament-dynamic-navigation.nav.groups.group', true)
            ? __(config('kda.filament-dynamic-navigation.nav.groups.group_lang_key'))
            : '';
    }
}
