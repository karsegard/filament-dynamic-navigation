<?php

namespace KDA\Filament\DynamicNavigation\Resources;

use KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource\Pages;
use KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource\RelationManagers;
use KDA\Filament\DynamicNavigation\Models\NavigationGroup;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource\RelationManagers\ItemsRelationManager;
use Filament\Facades\Filament;
use Illuminate\Support\HtmlString;
use KDA\Filament\DynamicNavigation\Resources\DynamicNavigationGroupResource\Pages\CreateDynamicNavigationGroup;

class DynamicNavigationGroupResource extends Resource
{
    protected static ?string $model = NavigationGroup::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
                TextInput::make('name')->dehydrateStateUsing(fn($state)=> blank($state) ? '' : $state),
                Select::make('registered_group')->options(function(){
                    return collect(Filament::getNavigation())->keys()->mapWithKeys(fn($item)=> [$item=>$item])->toArray();
                })->reactive()
                ->afterStateUpdated(fn($set,$get)=>$set('name',$get('registered_group')))
                ->dehydrated(false)
                ->visible(fn($livewire)=>$livewire instanceof CreateDynamicNavigationGroup),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')->formatStateUsing(function($state){
                    if(blank($state)){
                        return new HtmlString("<i>[Root]</i>");
                    }
                    return $state;
                }),
                TextColumn::make('items_count')->counts('items')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            ItemsRelationManager::class
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListDynamicNavigationGroups::route('/'),
            'create' => Pages\CreateDynamicNavigationGroup::route('/create'),
            'edit' => Pages\EditDynamicNavigationGroup::route('/{record}/edit'),
        ];
    }    
}
