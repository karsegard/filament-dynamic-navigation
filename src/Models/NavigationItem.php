<?php

namespace KDA\Filament\DynamicNavigation\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use \KDA\Filament\DynamicNavigation\Database\Factories\NavigationItemFactory;

class NavigationItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'label',
        'resource',
        'page',
        'group_id',
        'icon',
        'sort',
        'url',
        'open_in_new_tab',
        'use_navigation_items'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       'use_navigation_items'=>'boolean'
    ];

   
    public function getTable()
    {
        return 'filament_navigation_items';
    }
    protected static function newFactory()
    {
        return NavigationItemFactory::new();
    }

    public function group(){
        return $this->belongsTo(NavigationGroup::class,'group_id');
    }

    public function getClassExistsAttribute(){
        return class_exists($this->resource,false);
    }
}
