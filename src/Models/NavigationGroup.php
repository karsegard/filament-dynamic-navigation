<?php

namespace KDA\Filament\DynamicNavigation\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use \KDA\Filament\DynamicNavigation\Database\Factories\NavigationGroupFactory;

class NavigationGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'collected',
        'sort'
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       'collected'=>'boolean'
    ];

    public function getTable()
    {
        return 'filament_navigation_groups';
    }
   
    protected static function newFactory()
    {
        return  NavigationGroupFactory::new();
    }

    public function items(){
        return $this->hasMany(NavigationItem::class,'group_id')->orderBy('sort');
    }

}
