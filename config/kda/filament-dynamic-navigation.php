<?php
// config for KDA/Filament\DynamicNavigation
return [
    'nav' => [
        'groups' => [
            'group_lang_key' => 'filament-dynamic-navigation::nav.group',
            'group' => true
        ],
        'items' => [
            'group_lang_key' => 'filament-dynamic-navigation::nav.group',
            'group' => true
        ]
    ]
];
