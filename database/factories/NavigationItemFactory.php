<?php

namespace KDA\Filament\DynamicNavigation\Database\Factories;

use KDA\Filament\DynamicNavigation\Models\NavigationItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class NavigationItemFactory extends Factory
{
    protected $model = NavigationItem::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
