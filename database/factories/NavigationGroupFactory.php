<?php

namespace KDA\Filament\DynamicNavigation\Database\Factories;

use KDA\Filament\DynamicNavigation\Models\NavigationGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class NavigationGroupFactory extends Factory
{
    protected $model = NavigationGroup::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
