<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('filament_navigation_items', function (Blueprint $table) {
            $table->id();
            $table->string('label')->nullable();
            $table->string('resource')->nullable();
            $table->string('page')->nullable();
            $table->boolean('use_navigation_items')->default(1);
            $table->string('url')->nullable();
            $table->boolean('open_in_new_tab')->default(false);
            $table->string('icon')->nullable();
            $table->integer('sort')->nullable();
            $table->foreignId('group_id')->nullable();
            $table->string('name')->virtualAs('COALESCE(resource,page,\'\')');
            $table->timestamps();

        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('navigation_items');
     
        Schema::enableForeignKeyConstraints();
    }
};